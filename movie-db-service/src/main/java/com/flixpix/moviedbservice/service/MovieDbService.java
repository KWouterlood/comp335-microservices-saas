package com.flixpix.moviedbservice.service;

import com.flixpix.moviedbservice.repository.MovieDbServiceRepository;
import com.flixpix.moviedbservice.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieDbService {

    @Autowired
    private MovieDbServiceRepository movieDbServiceRepository;

    public List<Movie> getAllMovies() {
        return movieDbServiceRepository.findAll();
    }

    // use the Jpa method save() to save movies to db
    public void saveMovie(Movie newMovie) {
        movieDbServiceRepository.save(newMovie);
    }

    public Movie findByMovieName(String movieName) {
        return movieDbServiceRepository.findByMovieName(movieName);
    }

    public Movie findByMovieId(Integer movieId) {
        return movieDbServiceRepository.findByMovieId(movieId);
    }
}
