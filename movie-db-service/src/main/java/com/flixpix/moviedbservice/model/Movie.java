package com.flixpix.moviedbservice.model;

import javax.persistence.*;

/**
 * Data Access Object to be used in Repository Interface
 */

@Entity
@Table(name = "movies_table")
public class Movie {

    @Id
    // This automatically generates a new ID.
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "movie_name")
    private String movieName;

    @Column(name = "release_date")
    private String releaseDate;


    public Movie() {

    }
    // Constructor
    public Movie(String movieName, String releaseDate) {
        this.movieName = movieName;
        this.releaseDate = releaseDate;
    }

    // Getters
    public Integer getId() {
        return this.id;
    }

    public String getMovieName() {
        return this.movieName;
    }

    public String getReleaseDate() {
        return this.releaseDate;
    }

    // Setters
    public void setId(Integer id) {
        this.id = id;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }
}
