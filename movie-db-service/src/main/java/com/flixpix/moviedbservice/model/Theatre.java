package com.flixpix.moviedbservice.model;

import javax.persistence.*;

/**
 * Data Access Object (Model) to be used in the Repository Interface
 */

@Entity
@Table(name = "theatres_table")
public class Theatre {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "theatre_name")
    private String theatreName;

    @Column(name = "location")
    private String location;

    // Constructors
    public Theatre() {

    }

    public Theatre(String theatreName, String location) {
        this.theatreName = theatreName;
        this.location = location;
    }

    // Getters
    public Integer getId() { return id; }

    public String getTheatreName() { return theatreName; }

    public String getLocation() { return location; }

    // Setters
    public void setId(Integer id) { this.id = id; }

    public void setTheatreName(String theatreName) { this.theatreName = theatreName; }

    public void setLocation(String location) { this.location = location; }


}
