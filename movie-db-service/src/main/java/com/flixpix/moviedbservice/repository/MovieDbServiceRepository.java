package com.flixpix.moviedbservice.repository;

import com.flixpix.moviedbservice.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MovieDbServiceRepository extends JpaRepository<Movie, Integer> {

    // make magic search happen
    @Query("select m from Movie m where m.movieName = ?1")
    Movie findByMovieName(@Param("movieName") String movieName);

    // find movie by ID
    @Query("select m from Movie m where m.id = ?1")
    Movie findByMovieId(@Param("movieId") Integer movieId);

}

