package com.flixpix.moviedbservice.controller;

import com.flixpix.moviedbservice.model.Movie;
import com.flixpix.moviedbservice.service.MovieDbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * This controller handles the /movies url
 */
@RestController
@RequestMapping("/movies")
public class MovieDbServiceController {

    // allows for magical bean injection
    @Autowired
    private MovieDbService movieDbService;

    // returns all movies in movie-db
    @GetMapping("/all")
    public List<Movie> getAllMovies() {
        return movieDbService.getAllMovies();
    }

    // test mapping
    @GetMapping("/test")
    public String test() {
        return "This is a lovely test page dumdeedum.";
    }

    /**
     * GET /create --> Create a new movie and save it in the database.
     */
    @RequestMapping("/create")
    @ResponseBody
    public String create(String movieName, String releaseDate){
        String movieID = "";
        try {
            // create new movie object
            Movie newMovie = new Movie(movieName, releaseDate);
            // use movie service to call jpa repo methods
            movieDbService.saveMovie(newMovie);
            // get db id of new movie
            movieID = String.valueOf(newMovie.getId());
        }
        catch (Exception e) {
            return "ERROR creating movie: " + e.toString();
        }
        return "Movie successfully created with ID = " + movieID;
    }

    /**
     * Get /get-movie-by-name -->
     */
    @GetMapping("/get-movie-by-name")
    @ResponseBody
    public String getMovieByName(String movieName) {
        String movieId = "";
        try {
            Movie movie = movieDbService.findByMovieName(movieName);
            movieId = String.valueOf(movie.getId());
        }
        catch (Exception e) {
            return "Movie not found " + e;
        }
        return "The movie ID is: " + movieId;
    }

    /**
     * GET /get-movie (ID)--> returns Movie db Object
     */
    @GetMapping("/get-movie")
    @ResponseBody
    public Movie getMovie(Integer movieId) {
        Movie movie = new Movie();
        try {
            movie = movieDbService.findByMovieId(movieId);
        }
        catch (Exception e) {
            // how to handle exception e returning object;
        }
        return movie;
    }
}
