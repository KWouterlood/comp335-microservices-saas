package com.flixpix.moviedbservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieDbServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(MovieDbServiceApplication.class, args);
	}
}
